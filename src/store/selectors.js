import { createSelector } from '@reduxjs/toolkit';

export const allSchedulledTripSelector = (state) => state.scheduledTripSlice.allScheduledTrips;
export const searchTripQuerySelector = (state) => state.scheduledTripSlice.searchTripQuery;
export const sortTripOrderSelector = (state) => state.scheduledTripSlice.sortTripOrder;
export const filteredAndSortedTripsSelector = createSelector([
	allSchedulledTripSelector,
	searchTripQuerySelector,
	sortTripOrderSelector,
], (allScheduledTrips, searchTripQuery, sortTripOrder) => {
	const sortedTrips = [...allScheduledTrips];
	if (sortTripOrder === 'default') {
		return sortedTrips.filter((trip) => trip?.city?.toLowerCase()
			.includes(searchTripQuery.toLowerCase()));
	}
	sortedTrips.sort((a, b) => {
		const dateA = new Date(a.startDate);
		const dateB = new Date(b.startDate);
		return sortTripOrder === 'desc' ? dateB - dateA : dateA - dateB;
	});
	return sortedTrips.filter((trip) => trip?.city?.toLowerCase()
		.includes(searchTripQuery.toLowerCase()));
});
