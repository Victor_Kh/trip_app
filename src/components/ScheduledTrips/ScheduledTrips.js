import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TripCard from '../TripCard/TripCard';
import NothingFound from '../NothingFound/NothingFound';
import useScroll from '../../hooks/useScroll';
import getWeather from '../../store/thunkAction';
import { allSchedulledTripSelector, filteredAndSortedTripsSelector } from '../../store/selectors';

const ScheduledTrips = () => {
	const filteredAndSortedTrips = useSelector(filteredAndSortedTripsSelector);
	const selectedTrip = useSelector(allSchedulledTripSelector).find((trip) => trip.isSelected);
	const sliderRef = useRef();
	const dispatch = useDispatch();

	useEffect(() => {
		if (selectedTrip) {
			dispatch(getWeather({
				city: selectedTrip.city, startDate: selectedTrip.startDate, endDate: selectedTrip.endDate, id: selectedTrip.id,
			}));
		}
	}, []);
	const { onNextButtonClick, onPreviousButtonClick, hasScroll } = useScroll(sliderRef, filteredAndSortedTrips);

	return (
		<>
			<ul className='scheduled_trips' ref={sliderRef}>
				{filteredAndSortedTrips.length ? (filteredAndSortedTrips.map(({
					city, startDate, endDate, imageUrl, isSelected, id,
				}) => {
					return (
						<TripCard
							key={id}
							city={city}
							image={imageUrl}
							startDate={startDate}
							endDate={endDate}
							isSelected={isSelected}
							id={id}
						/>
					);
				})) : <NothingFound />}
			</ul>
			{hasScroll && <div className='slider_btns'>
				<button className='slider_btns__btn' onClick={onPreviousButtonClick}>Prev</button>
				<button className='slider_btns__btn' onClick={onNextButtonClick}>Next</button>
			</div>}
		</>
	);
};

export default ScheduledTrips;
